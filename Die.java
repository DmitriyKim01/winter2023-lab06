import java.util.Random;

public class Die{
	private int faceValue;
	private Random random;

	public Die(){
		this.faceValue = 1;
		random = new Random();
	}

	public int getFaceValue(){
		return this.faceValue;
	}

	public void roll(){
		this.faceValue = random.nextInt(6)+1;
	}

	public String toString(){
		return "Die number: "+this.faceValue;
	}
}

