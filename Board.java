public class Board{
	private Die die01;
	private Die die02;
	boolean[] tiles;

	public Board(){
		this.die01 = new Die();
		this.die02 = new Die();
		this.tiles = new boolean[12];
	}

	public String toString(){
		String result = "----------------------------------------------------\n| ";
		for (int i = 0; i < tiles.length; i++){
			if (!tiles[i]){
				result += i + 1 + " | ";
			}
			else{
				result += "X | ";
			}
		}
		return result + "\n----------------------------------------------------";
	}

	public boolean playATurn(){
		die01.roll();
		die02.roll();
		System.out.println(die01);
		System.out.println(die02);
		int sumOfDice = die01.getFaceValue() + die02.getFaceValue();

		if (!tiles[sumOfDice-1]){
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		if (!tiles[die01.getFaceValue()-1]){
			tiles[die01.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: " + die01.getFaceValue());
			return false;
		}
		if (!tiles[die02.getFaceValue()-1]){
			tiles[die02.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: " + die02.getFaceValue());
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut!");
		return true;
	}
}
