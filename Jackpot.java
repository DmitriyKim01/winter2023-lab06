import java.util.Scanner;

public class Jackpot{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome on my super fancy board game!");
		
		int winsCounter = 0;
		char button = 'r';
		
		do{
			Board board = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
		
			while (!gameOver){
			System.out.println(board);
			if (board.playATurn()){
				gameOver =true;
			}
			else{
				numOfTilesClosed += 1;
			}
		}
		if (numOfTilesClosed >= 7){
			System.out.println("You won jackpot!");
			winsCounter++;
		}
		else{
			System.out.println("You lost!");
		}
			
			System.out.println("\nWould you like to play another game?");
			System.out.println("Options: (r, restart), (q, quit)");
			button = scan.next().charAt(0);
		}while(button != 'q');
		System.out.println("You won: " + winsCounter + " times!");
	}
}
